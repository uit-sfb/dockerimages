#!/usr/bin/env bash

set -e

TAG="$1"
SPARK_VERSION=2.4.0

DIR="$( cd "$( dirname "$0" )" && pwd )"
 
docker build -t registry.gitlab.com/uit-sfb/dockerimages/spark_${SPARK_VERSION}:${TAG} --build-arg SPARK_VERSION=${SPARK_VERSION} ${DIR}
docker push registry.gitlab.com/uit-sfb/dockerimages/spark_${SPARK_VERSION}:${TAG}
