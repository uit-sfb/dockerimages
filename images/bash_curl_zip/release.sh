#!/usr/bin/env bash

set -e

TAG="$1"

DIR="$( cd "$( dirname "$0" )" && pwd )"
 
docker build -t registry.gitlab.com/uit-sfb/dockerimages/bash_curl_zip:${TAG} ${DIR}
docker push registry.gitlab.com/uit-sfb/dockerimages/bash_curl_zip:${TAG}
