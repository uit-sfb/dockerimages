#!/usr/bin/env bash

set -e

TAG="$1"
SBT_VERSION=1.2.8

DIR="$( cd "$( dirname "$0" )" && pwd )"
 
docker build -t registry.gitlab.com/uit-sfb/dockerimages/docker_from_host_sbt_${SBT_VERSION}:${TAG} ${DIR}
docker push registry.gitlab.com/uit-sfb/dockerimages/docker_from_host_sbt_${SBT_VERSION}:${TAG}
